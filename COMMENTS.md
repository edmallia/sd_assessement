# Assessment Project Comments And Assumptions

This project is in fulfillment of the Tipico Software Development Assessment Project - Java, Spring MVC, AngularJS.

This page describes how the project structure, the RESTful API used by the client side app to fetch and create notes, and finally provides build and deployment instructions.

## Project Structure
The project has been split into two maven projects with a parent owning both as shown below.

    assessement-parent
    |____ assessement-angular
    |____ assessement-war

## assessement-angular
The assessement-angular project contains a npm and bower managed javascript app that has been packaged within a maven project with the help of the [frontend-maven-plugin](https://github.com/eirslett/frontend-maven-plugin).  The plugin will download the specified versions of node and npm (specified as properties in the parent pom) and in the appropriate maven lifecycle phases, it will:
* Perform 'npm install' to install the required modules
* This will in turn invoke 'bower install' in order to place the required dependencies in the app folder
* Run the JavaScript tests

Using this plugin, we can run both Java and JavaScript tests from within Maven, while still allowing for npm and bower to be used independently during development.

This project has been setup to spew out a [web-jar](http://www.webjars.org/) with the packaged JavaScript app.

## assessement-war

The assessment-war project presents a RESTful HTTP API using JSON as the data bearer and implemented using Spring MVC. The API is defined in the API Description section below.  Internally, assessement-war uses Spring Data Repositories, JPA and Hibernate to save and fetch the Notes.

As one of its dependencies, the assessment-war has the assessement-angular project.  As described above, the assessement-angular project has been packaged as a web-jar and exposed using Spring MVC resource mapping.

The tests for the assessement-war project are using Spring Test utilties to set up an empty H2 database with the relevant schema, in order to test the Spring Data Repository.  

## API Description
### Note Creation
#### Request
    URI: POST api/notes
    Body: { "text" : "body text here"}
    Content-Type: application/json
#### Response
    HTTP Status: 201 CREATED
    Content-Type: application/json
    Body: { "id" : 152, "createdDate" : "10-09-2015 00:04:50", "text" : "body text here" }
#### Error Response
    HTTP Status: 400 Bad Request
    Content-Type: application/json
    Body on missing text field: { "message" : "A note text is required to create a Note." }
    Body on incorrect length: { "message" : "Notes text should be between 1 and 100 characters long." }

### Note Fetching
#### Request
    URI: GET api/notes
    Params: pageSize, pageNumber, ascending
#### Response
    HTTP Status: 200 OK
    Content-Type: application/json
    Body: {"notes" : [], "totalElements" : 0, "totalPages" : 0, "pageNumber" : 0, "elementsOnPage" : 0, "requestedPageSize" : 10, "createdDateAscendingOrder" : false }

## Database
The database schema is available in the project at assessment-war/src/test/resources/db/create.sql.  The script file creates a schema called TIPCIO (if it doesn't exist, and creates a table notes within that schema.

## Building and running the project
As mentioned before, the project structure allows for building, testing and packaging of the JavaScript app and the Java application through a single 'mvn verify' command issued on the assessement-parent.

In order to run the application, kindly follow these steps:

1. Create the TIPICO schema and the required notes table by executing the SQL script file assessment-war/src/test/resources/db/create.sql
1. Inspect filter.properties and ensure that the JDBC connection URL, username and password are set correctly
1. Run 'mvn clean verify' on the parent project (Note: On first build, the procoess may take a long time round while node and npm are downloaded)
1. Deploy the produced war file assessment-war/target/notes.war to Tomcat
1. The app will be available at /notes/app/index.html e.g. http://localhost:8080/notes/app/index.html
