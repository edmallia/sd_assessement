# Assessment Project - Java, Spring MVC, AngularJS

Welcome to the Tipico Software Development Assessment Project - Java, Spring MVC, AngularJS

Here you find a short summary of your tasks. Please use this project "sd_assessment" as your skeleton and add new sources to complete the tasks. 
Follow the **Development and Submission steps** in order to do the below **Tasks** and submit them to us for review.

## Tasks

Create a new site where users are able to:

1. Create notes via a web based page having the ability to create a new note through an input field.
2. Notes can be submitted to the server by clicking a button "Send" which does its work by utilizing AngularJS.
3. All created notes are to displayed below the input field, sorted by date.
4. Notes can be sorted ascending or descending by its creation date.
5. Notes have a max length of 100 characters.
6. Notes must be saved on the server (mySQL DB, schema titled TIPICO) so that they can survive a reboot.
7. Unit tests should be available to test saving and loading notes as well as any other logic you see worth testing (Java and Angular).
8. The procedure of saving and loading a note should take place in a spring bean (eg. a service bean).
9. The list of notes below the input field should refresh to reflect changes.
10. The list of created notes should be pageable, showing 10 notes at a time, with the ability to view other pages in case there are more than 10 notes.

## Development and Submission steps

1. Clone the master branch to your local machine
2. Please commit frequently to your local git repository (no need to push)
3. When ready, make sure everything is committed and clean the target folder
4. Add any assumptions or notes to the file named COMMENTS.md
5. Compress your solution in ZIP format, upload to a cloud service and send us the link for us to be able to download the solution.


**Good luck!**