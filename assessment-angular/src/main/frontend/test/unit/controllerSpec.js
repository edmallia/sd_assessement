'use strict';

/* jasmine specs for controllers go here */
describe('Notes Controllers', function() {

    beforeEach(function() {
        this.addMatchers({
            toEqualData: function(expected) {
                return angular.equals(this.actual, expected);
            }
        });
    });

    beforeEach(module('TipicoNotesApp'));

    describe('notesTableController', function() {
        var scope, rootScope, deferred, ctrl, $httpBackend;

        beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
            $httpBackend = _$httpBackend_;
            $httpBackend.expectGET('../api/notes?ascending=true&pageNumber=0&pageSize=10').
            respond({
                "notes": [],
                "totalElements": 0,
                "totalPages": 0,
                "pageNumber": 0,
                "elementsOnPage": 0,
                "requestedPageSize": 10,
                "createdDateAscendingOrder": false
            });

            scope = $rootScope.$new();
            rootScope = $rootScope;

            ctrl = $controller('notesTableController', {
                $scope: scope
            });
        }));

        it('should have the initialisation set', function() {
            expect(scope.init).toEqualData({
                "thead": true,
                "pagination": true,
                "count": 10,
                "page": 1,
                "sortBy": "createdDate",
                "sortOrder": "asc"
            });
        });

    });

    describe('notesInputController', function() {
        var scope, rootScope, deferred, ctrl, $httpBackend, notesTableController;

        beforeEach(inject(function(_$httpBackend_, $rootScope, $controller, $injector) {
            $httpBackend = _$httpBackend_;
            $httpBackend.expectPOST('../api/notes').
            respond({
                "id": 1,
                "createdDate": "09-09-2015 00:05:50",
                "text": "green green"
            });

            scope = $rootScope.$new();
            rootScope = $injector.get('$rootScope');
            spyOn(rootScope, '$broadcast');
            ctrl = $controller('notesInputController', {
                $scope: scope
            });


        }));

        it('should have an empty note text data', function() {
            expect(scope.noteText).toEqualData('');
        });

        it('should allow the creation of a note', function() {
            scope.noteText = "green green";
            scope.addNote();

            //expect(rootScope.$broadcast).toHaveBeenCalledWith('noteSaved');
        });

    });

});