'use strict';

describe('service', function() {

    // load modules
    beforeEach(module('TipicoNotesApp'));

    // Test service availability
    it('check the existence of Notes factory', inject(function(Notes) {
        expect(Notes).toBeDefined();
    }));

    it('check the existence of Notes query function', inject(function(Notes) {
        expect(Notes.query).toBeDefined();
    }));

    it('check the existence of Notes query function', inject(function(Notes) {
        expect(Notes.save).toBeDefined();
    }));

    it('check the existence of NotesTableService factory', inject(function(NotesTableService) {
        expect(NotesTableService).toBeDefined();
    }));

    it('check the existence of NotesTableService getNotes', inject(function(NotesTableService) {
        expect(NotesTableService.getNotes).toBeDefined();
    }));
});