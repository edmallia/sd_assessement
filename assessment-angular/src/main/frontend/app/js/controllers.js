'use strict';

/* Controllers */

var notesControllers = angular.module('notesControllers', ['ngTasty']);


notesControllers.controller('notesInputController', function($scope, $rootScope, Notes) {
    $scope.showAlert = false;
    $scope.doFade = false;
    $scope.alertMessage = '';
    $scope.isErrorAlert = false;

    $scope.hideAlert = function() {
        $scope.doFade = true;
        $scope.showAlert = false;
    };

    $scope.noteText = '';

    $scope.addNote = function() {
        Notes.save('../api/notes', {
                text: $scope.noteText
            },
            function(response) {
                // this callback will be called asynchronously
                // when the response is available
                $scope.noteText = '';

                //reset
                $scope.showAlert = false;
                $scope.doFade = false;

                $scope.showAlert = true;
                $scope.isErrorAlert = false;
                $scope.alertMessage = 'Note added successfully';

                //send broadcast for notesTableController to update the table
                $rootScope.$broadcast('noteSaved');

            },
            function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.

                //reset
                $scope.showAlert = false;
                $scope.doFade = false;

                $scope.showAlert = true;
                $scope.isErrorAlert = true;
                $scope.alertMessage = 'Error: ' + response.data.message;
            });
    };

});

notesControllers.controller('notesTableController', function($scope, $rootScope, NotesTableService) {

    $scope.init = {
        "thead": true,
        "pagination": true,
        "count": 10,
        "page": 1,
        "sortBy": "createdDate",
        "sortOrder": "asc"
    };

    $scope.notSortBy = ['id', 'text'];
    $scope.listItemsPerPage = [];
    $scope.reloadCallback = function() {};

    $rootScope.$on('noteSaved', function() {
        $scope.reloadCallback();
    });

    $scope.customTheme = {
        loadOnInit: true,
        bootstrapIcon: true
    };

    $scope.getResource = function(ngTastyTableParams, ngTastyTableParamsObj) {

        var queryParams = {
            pageSize: 10,
            pageNumber: ngTastyTableParamsObj.page - 1, //indexing on our server starts from zero, but in the app need to be shown from 1
            ascending: (ngTastyTableParamsObj.sortOrder == 'asc') ? true : false
        };

        return NotesTableService.getNotes(10, ngTastyTableParamsObj.page, (ngTastyTableParamsObj.sortOrder == 'asc') ? true : false);
    };
});