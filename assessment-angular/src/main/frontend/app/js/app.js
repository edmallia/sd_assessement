'use strict';

/* App Module */

var TipicoNotesApp = angular.module('TipicoNotesApp', [
    'ngRoute',
    'ngTasty',
    'ngResource',
    'notesControllers',
    'notesServices'
]);