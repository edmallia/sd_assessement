var notesServices = angular.module('notesServices', ['ngResource']);

notesServices.factory('NotesTableService', ['Notes',
    function(Notes) {
        return {
            getNotes: function(pageSize, pageNumber, ascending) {
                var queryParams = {
                    pageSize: pageSize,
                    pageNumber: pageNumber - 1, //indexing on our server starts from zero, but in the app need to be shown from 1
                    ascending: ascending
                };

                return Notes.query(queryParams).$promise.then(function(data) {
                    var ret = {
                        'rows': data.notes,
                        'header': [
                            //uncomment to add the id to the table
                            // {
                            //     'key': 'id',
                            //     'name': 'Id'
                            // },
                            {
                                'key': 'createdDate',
                                'name': 'Created Date'
                            }, {
                                'key': 'text',
                                'name': 'Note'
                            }
                        ],
                        'pagination': {
                            'count': data.pageSize,
                            'page': (data.pageNumber + 1), //indexing on our server starts from zero, but in the app need to be shown from 1
                            'pages': data.totalPages,
                            'size': data.totalElements
                        },
                        'sortBy': ['createdDate'],
                        'sortOrder': ascending ? "asc" : "dsc"
                    };
                    return ret;
                });
            }
        }
    }
]);

notesServices.factory('Notes', ['$resource',
    function($resource) {
        return $resource('../api/notes', {}, {
            query: {
                method: 'GET',
                params: {
                    pageSize: 10,
                    pageNumber: 0,
                    ascending: true
                },
                isArray: false
            },
            save: {
                method: 'POST'
            },
        });
    }
]);