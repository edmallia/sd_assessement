package com.tipico.assessment.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tipico.assessment.dtos.CreateNoteRequest;
import com.tipico.assessment.entities.Note;
import com.tipico.assessment.repositories.NoteRepository;
import org.hamcrest.CustomTypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.isA;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by dwardu on 08/09/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:spring/api-controller-test-context.xml")
public class ApiControllerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiControllerTest.class);

    @Inject
    private WebApplicationContext wac;

    @Inject
    NoteRepository noteRepository;

    ObjectMapper objectMapper = new ObjectMapper();

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

        noteRepository.deleteAll();
    }

    @Test
    public void testCreateNote() throws Exception {

        CreateNoteRequest createNoteRequest = new CreateNoteRequest();
        createNoteRequest.setText("Hello, world!");

        final Date now = new Date();

        this.mockMvc.perform(
                post("/api/notes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(createNoteRequest))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value("Hello, world!"))
                .andExpect(jsonPath("$.createdDate").exists())
                .andExpect(jsonPath("$.createdDate").value(new CustomTypeSafeMatcher<String>("Created Date not in expected format or after service invocation") {
                    @Override
                    protected boolean matchesSafely(String s) {
                        try {
                            //get time without millis
                            long time = now.getTime() - (now.getTime() % 1000);

                            Date parsed = formatter.parse(s);
                            if (time <= parsed.getTime()) {
                                return true;
                            }
                        } catch (ParseException e) {
                            LOGGER.error("Created Date " + s + "is not in correct format", e);
                            return false;
                        }
                        return false;
                    }
                }))
                .andExpect(jsonPath("$.id").value(isA(Number.class)))
        ;

    }

    @Test
    public void testCreateNoteEmptyBody() throws Exception {

        this.mockMvc.perform(
                post("/api/notes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}")
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value(CreateNoteRequest.VALIDATION_MESSAGE_NULL_TEXT))
        ;

    }

    @Test
    public void testCreateNoteEmptyText() throws Exception {

        CreateNoteRequest createNoteRequest = new CreateNoteRequest();
        createNoteRequest.setText("");

        this.mockMvc.perform(
                post("/api/notes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(createNoteRequest))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value(CreateNoteRequest.VALIDATION_MESSAGE_INVALID_LENGTH))
        ;

    }

    @Test
    public void testCreateNoteTooLong() throws Exception {

        CreateNoteRequest createNoteRequest = new CreateNoteRequest();
        String longText = "";
        for (int i = 0; i < 10; i++){
            longText+="1234567890";
        }
        longText+="X";
        createNoteRequest.setText(longText);

        this.mockMvc.perform(
                post("/api/notes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(createNoteRequest))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value(CreateNoteRequest.VALIDATION_MESSAGE_INVALID_LENGTH))
        ;

    }


    @Test
    public void testLoadEmpty() throws Exception {
        this.mockMvc.perform(
                get("/api/notes")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("pageSize", "10")
                        .param("pageNumber", "0")
                        .param("ascending", "true")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.totalPages").value(0))
                .andExpect(jsonPath("$.pageNumber").value(0))
                .andExpect(jsonPath("$.elementsOnPage").value(0))
                .andExpect(jsonPath("$.requestedPageSize").value(10))
                .andExpect(jsonPath("$.createdDateAscendingOrder").value(true))
                .andExpect(jsonPath("$.notes", hasSize(0)));
    }


    @Test
    public void testLoad() throws Exception {


        //simulate creation in the past
        Date now = new Date();
        Long time = now.getTime() - 60000;//less 60 secs

        //create 55 notes
        for (int i = 0; i < 55; i++) {

            Note note = new Note();
            note.setCreatedDate(new Date(time));
            time += 1000; // advance by one second
            note.setText("N " + i);

            noteRepository.save(note);
        }

        //first page asc
        this.mockMvc.perform(
                get("/api/notes")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("pageSize", "10")
                        .param("pageNumber", "0")
                        .param("ascending", "true")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalElements").value(55))
                .andExpect(jsonPath("$.totalPages").value(6))
                .andExpect(jsonPath("$.pageNumber").value(0))
                .andExpect(jsonPath("$.elementsOnPage").value(10))
                .andExpect(jsonPath("$.requestedPageSize").value(10))
                .andExpect(jsonPath("$.createdDateAscendingOrder").value(true))
                .andExpect(jsonPath("$.notes", hasSize(10)))
                .andExpect(jsonPath("$.notes.[*].text", containsInAnyOrder("N 0", "N 1", "N 2", "N 3", "N 4", "N 5", "N 6", "N 7", "N 8", "N 9")));

        //last page asc
        this.mockMvc.perform(
                get("/api/notes")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("pageSize", "10")
                        .param("pageNumber", "5")
                        .param("ascending", "true")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalElements").value(55))
                .andExpect(jsonPath("$.totalPages").value(6))
                .andExpect(jsonPath("$.pageNumber").value(5))
                .andExpect(jsonPath("$.elementsOnPage").value(5))
                .andExpect(jsonPath("$.requestedPageSize").value(10))
                .andExpect(jsonPath("$.createdDateAscendingOrder").value(true))
                .andExpect(jsonPath("$.notes", hasSize(5)))
                .andExpect(jsonPath("$.notes.[*].text", containsInAnyOrder("N 50", "N 51", "N 52", "N 53", "N 54")));


        //first page desc
        this.mockMvc.perform(
                get("/api/notes")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("pageSize", "10")
                        .param("pageNumber", "0")
                        .param("ascending", "false")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalElements").value(55))
                .andExpect(jsonPath("$.totalPages").value(6))
                .andExpect(jsonPath("$.pageNumber").value(0))
                .andExpect(jsonPath("$.elementsOnPage").value(10))
                .andExpect(jsonPath("$.requestedPageSize").value(10))
                .andExpect(jsonPath("$.createdDateAscendingOrder").value(false))
                .andExpect(jsonPath("$.notes", hasSize(10)))
                .andExpect(jsonPath("$.notes.[*].text", containsInAnyOrder("N 54", "N 53", "N 52", "N 51", "N 50", "N 49", "N 48", "N 47", "N 46", "N 45")));

        //last page desc
        this.mockMvc.perform(
                get("/api/notes")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("pageSize", "10")
                        .param("pageNumber", "5")
                        .param("ascending", "false")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalElements").value(55))
                .andExpect(jsonPath("$.totalPages").value(6))
                .andExpect(jsonPath("$.pageNumber").value(5))
                .andExpect(jsonPath("$.elementsOnPage").value(5))
                .andExpect(jsonPath("$.requestedPageSize").value(10))
                .andExpect(jsonPath("$.createdDateAscendingOrder").value(false))
                .andExpect(jsonPath("$.notes", hasSize(5)))
                .andExpect(jsonPath("$.notes.[*].text", containsInAnyOrder("N 4", "N 3", "N 2", "N 1", "N 0")));
    }

    @After
    public void teardown(){
        noteRepository.deleteAll();
    }


}
