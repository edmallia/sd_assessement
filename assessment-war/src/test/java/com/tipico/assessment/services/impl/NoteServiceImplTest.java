package com.tipico.assessment.services.impl;

import com.tipico.assessment.dtos.CreateNoteRequest;
import com.tipico.assessment.dtos.PageOfNotes;
import com.tipico.assessment.entities.Note;
import com.tipico.assessment.repositories.NoteRepository;
import com.tipico.assessment.services.NoteService;
import org.hamcrest.CustomTypeSafeMatcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import java.util.Date;

/**
 * Created by dwardu on 07/09/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/note-service-impl-test-context.xml")
public class NoteServiceImplTest {

    @Inject
    NoteService noteService;

    @Inject
    NoteRepository noteRepository;

    @Before
    public void init() {
        noteRepository.deleteAll();
    }

    @Test
    public void testCreateNote() throws InterruptedException {
        CreateNoteRequest createNoteRequest = new CreateNoteRequest();
        createNoteRequest.setText("The first note");

        final Date beforeSave = new Date();
        Note saved = noteService.createNote(createNoteRequest);

        Assert.assertNotNull(saved);
        Assert.assertNotNull(saved.getId());

        Assert.assertNotNull(saved.getText());
        Assert.assertEquals(createNoteRequest.getText(), saved.getText());

        Assert.assertNotNull(saved.getCreatedDate());
        String compareDateErrorMessage = "Created Date is not after Before Save Date [" + beforeSave + "]";
        Assert.assertThat(saved.getCreatedDate(), new CustomTypeSafeMatcher<Date>(compareDateErrorMessage) {
            @Override
            protected boolean matchesSafely(Date date) {
                if (beforeSave.getTime() <= date.getTime()) {
                    return true;
                }
                return false;
            }
        });
    }

    @Test
    public void testLoadEmpty() {
        PageOfNotes notes = noteService.loadNotes((short) 10, 0, true);

        Assert.assertNotNull(notes);
        Assert.assertNotNull(notes.getNotes());
        Assert.assertEquals(0, notes.getNotes().size());
        Assert.assertEquals(0, notes.getTotalElements());
        Assert.assertEquals(0, notes.getTotalPages());
        Assert.assertEquals(0, notes.getElementsOnPage());
        Assert.assertEquals(0, notes.getPageNumber());
        Assert.assertEquals(10, notes.getRequestedPageSize());
        Assert.assertEquals(true, notes.isCreatedDateAscendingOrder());

    }


    @Test
    public void testLoad() throws InterruptedException {

        //simulate creation in the past
        Date now = new Date();
        Long time = now.getTime() - 60000;//less 60 secs

        //create 5 notes
        for (int i = 0; i < 5; i++){

            Note note = new Note();
            note.setCreatedDate(new Date(time));
            time += 1000; // advance by one second
            note.setText("Note number " + i);

            noteRepository.save(note);
        }

        //load first page of 2 in ascending order
        PageOfNotes notes = noteService.loadNotes((short) 2, 0, true);
        Assert.assertNotNull(notes);
        Assert.assertNotNull(notes.getNotes());
        Assert.assertEquals(2, notes.getNotes().size());
        Assert.assertEquals(5, notes.getTotalElements());
        Assert.assertEquals(3, notes.getTotalPages());
        Assert.assertEquals(2, notes.getElementsOnPage());
        Assert.assertEquals(0, notes.getPageNumber());
        Assert.assertEquals(2, notes.getRequestedPageSize());
        Assert.assertEquals(true, notes.isCreatedDateAscendingOrder());
        Assert.assertEquals("Note number 0", notes.getNotes().get(0).getText());
        Assert.assertEquals("Note number 1", notes.getNotes().get(1).getText());

        //load second page of 2 in ascending order
        notes = noteService.loadNotes((short) 2, 1, true);
        Assert.assertNotNull(notes);
        Assert.assertNotNull(notes.getNotes());
        Assert.assertEquals(2, notes.getNotes().size());
        Assert.assertEquals(5, notes.getTotalElements());
        Assert.assertEquals(3, notes.getTotalPages());
        Assert.assertEquals(2, notes.getElementsOnPage());
        Assert.assertEquals(1, notes.getPageNumber());
        Assert.assertEquals(2, notes.getRequestedPageSize());
        Assert.assertEquals(true, notes.isCreatedDateAscendingOrder());
        Assert.assertEquals("Note number 2", notes.getNotes().get(0).getText());
        Assert.assertEquals("Note number 3", notes.getNotes().get(1).getText());

        //load third page of 2 in ascending order
        notes = noteService.loadNotes((short) 2, 2, true);
        Assert.assertNotNull(notes);
        Assert.assertNotNull(notes.getNotes());
        Assert.assertEquals(1, notes.getNotes().size());
        Assert.assertEquals(5, notes.getTotalElements());
        Assert.assertEquals(3, notes.getTotalPages());
        Assert.assertEquals(1, notes.getElementsOnPage());
        Assert.assertEquals(2, notes.getPageNumber());
        Assert.assertEquals(2, notes.getRequestedPageSize());
        Assert.assertEquals(true, notes.isCreatedDateAscendingOrder());
        Assert.assertEquals("Note number 4", notes.getNotes().get(0).getText());


        //load first page of 2 in descending order
        notes = noteService.loadNotes((short) 2, 0, false);
        Assert.assertNotNull(notes);
        Assert.assertNotNull(notes.getNotes());
        Assert.assertEquals(2, notes.getNotes().size());
        Assert.assertEquals(5, notes.getTotalElements());
        Assert.assertEquals(3, notes.getTotalPages());
        Assert.assertEquals(2, notes.getElementsOnPage());
        Assert.assertEquals(0, notes.getPageNumber());
        Assert.assertEquals(2, notes.getRequestedPageSize());
        Assert.assertEquals(false, notes.isCreatedDateAscendingOrder());
        Assert.assertEquals("Note number 4", notes.getNotes().get(0).getText());
        Assert.assertEquals("Note number 3", notes.getNotes().get(1).getText());


        //load second page of 2 in descending order
        notes = noteService.loadNotes((short) 2, 1, false);
        Assert.assertNotNull(notes);
        Assert.assertNotNull(notes.getNotes());
        Assert.assertEquals(2, notes.getNotes().size());
        Assert.assertEquals(5, notes.getTotalElements());
        Assert.assertEquals(3, notes.getTotalPages());
        Assert.assertEquals(2, notes.getElementsOnPage());
        Assert.assertEquals(1, notes.getPageNumber());
        Assert.assertEquals(2, notes.getRequestedPageSize());
        Assert.assertEquals(false, notes.isCreatedDateAscendingOrder());
        Assert.assertEquals("Note number 2", notes.getNotes().get(0).getText());
        Assert.assertEquals("Note number 1", notes.getNotes().get(1).getText());

        //load third page of 2 in descending order
        notes = noteService.loadNotes((short) 2, 2, false);
        Assert.assertNotNull(notes);
        Assert.assertNotNull(notes.getNotes());
        Assert.assertEquals(1, notes.getNotes().size());
        Assert.assertEquals(5, notes.getTotalElements());
        Assert.assertEquals(3, notes.getTotalPages());
        Assert.assertEquals(1, notes.getElementsOnPage());
        Assert.assertEquals(2, notes.getPageNumber());
        Assert.assertEquals(2, notes.getRequestedPageSize());
        Assert.assertEquals(false, notes.isCreatedDateAscendingOrder());
        Assert.assertEquals("Note number 0", notes.getNotes().get(0).getText());

    }

}
