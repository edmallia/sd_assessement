CREATE SCHEMA IF NOT EXISTS TIPICO;

CREATE TABLE TIPICO.note (
    id BIGINT NOT NULL AUTO_INCREMENT,
    created_date TIMESTAMP NOT NULL,
    note_text VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE INDEX notes_created_date_idx
    ON TIPICO.note(created_date);