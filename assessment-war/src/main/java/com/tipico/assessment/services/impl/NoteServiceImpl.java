package com.tipico.assessment.services.impl;

import com.tipico.assessment.dtos.CreateNoteRequest;
import com.tipico.assessment.dtos.PageOfNotes;
import com.tipico.assessment.entities.Note;
import com.tipico.assessment.repositories.NoteRepository;
import com.tipico.assessment.services.NoteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Date;

/**
 * An implementation of {@link NoteService} which uses {@link NoteRepository} to create and load the notes.
 * Created by dwardu on 07/09/15.
 */
@Component
public class NoteServiceImpl implements NoteService{

    @Inject
    NoteRepository noteRepository;

    @Override
    public PageOfNotes loadNotes(Short pageSize, Integer pageNumber, Boolean ascending) {

        //define the sorting order for createDate
        Sort.Direction direction = Sort.Direction.ASC;
        if ((ascending != null) && (!ascending)) {
            direction = Sort.Direction.DESC;
        }

        //create a paged request
        PageRequest pageable = new PageRequest(pageNumber, pageSize, direction, "createdDate");

        //load the notes and return
        Page<Note> page = noteRepository.findAll(pageable);

        return new PageOfNotes(page);
    }

    @Override
    public Note createNote(CreateNoteRequest createNoteRequest) {
        Note note = new Note();
        note.setText(createNoteRequest.getText());
        //set created date to now
        note.setCreatedDate(new Date());

        //save the supplied note
        return noteRepository.save(note);
    }
}
