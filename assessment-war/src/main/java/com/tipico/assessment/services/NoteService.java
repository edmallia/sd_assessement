package com.tipico.assessment.services;

import com.tipico.assessment.dtos.CreateNoteRequest;
import com.tipico.assessment.dtos.PageOfNotes;
import com.tipico.assessment.entities.Note;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * The NoteService that allows for the loading and the creation of {@link Note}
 * Created by dwardu on 07/09/15.
 */
public interface NoteService {

    /**
     * Loads paginated notes with the given pageSize for the given pageNumber and with the supplied ordering.  The
     * ordering is applied to {@link Note#createdDate}.
     *
     * @param pageSize The page size to be loaded i.e. the maximum number of notes to load
     * @param pageNumber The page number to load
     * @param ascending The order to be applied to {@link com.tipico.assessment.entities.Note#createdDate}
     * @return Page of notes
     */
    PageOfNotes loadNotes(Short pageSize, Integer pageNumber, Boolean ascending);

    /**
     * Create a new Note
     * @param createNoteRequest The new note to be created
     * @return The newly created note.
     */
    Note createNote(CreateNoteRequest createNoteRequest);
}
