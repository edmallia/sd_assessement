package com.tipico.assessment.repositories;

import com.tipico.assessment.entities.Note;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * The Spring Data Repository that manasges the {@link Note} entity.
 * Created by dwardu on 07/09/15.
 */
public interface NoteRepository extends PagingAndSortingRepository<Note, Long> {
}