package com.tipico.assessment.entities;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tipico.assessment.dtos.JsonDateSerializer;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by dwardu on 07/09/15.
 */
@Entity
@Table(name="note", schema="TIPICO")
public class Note {

    private Long id;

    private Date createdDate;

    private String text;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional=false)
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic(optional=false)
    @Column(name="created_date")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using=JsonDateSerializer.class)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic(optional=false)
    @Column(name="note_text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", createdDate=" + createdDate +
                ", text='" + text + '\'' +
                '}';
    }
}
