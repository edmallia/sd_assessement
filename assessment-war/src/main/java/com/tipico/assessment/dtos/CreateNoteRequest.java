package com.tipico.assessment.dtos;


import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A DTO for the create note request with validation support
 * Created by dwardu on 07/09/15.
 */
@Validated
public class CreateNoteRequest implements Serializable {

    public static final String VALIDATION_MESSAGE_NULL_TEXT = "A note text is required to create a Note.";
    public static final String VALIDATION_MESSAGE_INVALID_LENGTH = "Notes text should be between 1 and 100 characters long.";

    @NotNull(message=VALIDATION_MESSAGE_NULL_TEXT)
    @Size(min = 1, max = 100, message = VALIDATION_MESSAGE_INVALID_LENGTH)
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}