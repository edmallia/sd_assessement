package com.tipico.assessment.dtos;

/**
 * Created by dwardu on 08/09/15.
 */
public class ErrorMessage {
    private final String message;

    public ErrorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}