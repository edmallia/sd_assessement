package com.tipico.assessment.dtos;

import com.tipico.assessment.entities.Note;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.io.Serializable;
import java.util.List;

/**
 * A representation of the Page to be sent by the API.
 * Created by dwardu on 07/09/15.
 */
public class PageOfNotes implements Serializable {

    private final List<Note> notes;
    private final long totalElements;
    private final int totalPages;
    private final int pageNumber;
    private final int elementsOnPage;
    private final int requestedPageSize;
    private final boolean createdDateAscendingOrder;

    public PageOfNotes (Page<Note> page){
        notes = page.getContent();
        totalElements = page.getTotalElements();
        totalPages = page.getTotalPages();
        pageNumber = page.getNumber();
        elementsOnPage = page.getNumberOfElements();
        requestedPageSize = page.getSize();

        if (page.getSort() != null){
            Sort.Order createdDate = page.getSort().getOrderFor("createdDate");

            createdDateAscendingOrder = createdDate.isAscending();
        }
        else{
            createdDateAscendingOrder = true;
        }
    }

    public List<Note> getNotes() {
        return notes;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getElementsOnPage() {
        return elementsOnPage;
    }

    public int getRequestedPageSize() {
        return requestedPageSize;
    }

    public boolean isCreatedDateAscendingOrder() {
        return createdDateAscendingOrder;
    }
}