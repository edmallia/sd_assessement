package com.tipico.assessment.controllers.api;

import com.tipico.assessment.dtos.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

/**
 * Created by dwardu on 08/09/15.
 */
@ControllerAdvice("com.tipico.assessment.controllers.api")
public class ApiControllerAdvice {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiControllerAdvice.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleException(Exception e)
    {
        LOGGER.error("An unexpected error occurred.", e);
        String message = "An unexpected error occurred.";
        return new ErrorMessage(message);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage handleMethodArgumentNotValidException(MethodArgumentNotValidException e){
        LOGGER.error("ValidationException caught", e);

        List<FieldError> errors = e.getBindingResult().getFieldErrors();
        String errorMessage = "";
        for (FieldError fieldError : errors) {
            errorMessage += fieldError.getDefaultMessage();
        }
        return new ErrorMessage(errorMessage);

    }
}
