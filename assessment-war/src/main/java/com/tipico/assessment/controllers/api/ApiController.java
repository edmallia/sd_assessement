package com.tipico.assessment.controllers.api;

import com.tipico.assessment.dtos.CreateNoteRequest;
import com.tipico.assessment.dtos.PageOfNotes;
import com.tipico.assessment.entities.Note;
import com.tipico.assessment.services.NoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;

/**
 * The API Controller exposes an API for creating notes and loading paginated notes. data will be sent and received as
 * JSON strings.
 * Created by dwardu on 07/09/15.
 */
@Controller
public class ApiController {

    /**
     * The logger for the ApiController
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiController.class);


    @Inject
    NoteService noteService;

    @RequestMapping(value = "/api/notes", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public PageOfNotes getNotes(@RequestParam Short pageSize, @RequestParam Integer pageNumber, @RequestParam Boolean ascending) {
        return noteService.loadNotes(pageSize, pageNumber, ascending);
    }


    @RequestMapping(value = "/api/notes", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Note createNote(@RequestBody @Valid CreateNoteRequest createNoteRequest) {
        Note toReturn = noteService.createNote(createNoteRequest);

        return toReturn;
    }
}
