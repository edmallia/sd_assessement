<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
	<title>Assessment Project - Java, Spring MVC, AngularJS</title>
	<meta charset="UTF-8">
</head>
<body>
<div class="readme file file-markup wiki-content">
    <h1 id="markdown-header-assessment-project-comments-and-assumptions">Assessment Project Comments And Assumptions</h1>
<p>This project is in fulfillment of the Tipico Software Development Assessment Project - Java, Spring MVC, AngularJS.</p>
<p>This page describes how the project structure, the RESTful API used by the client side app to fetch and create notes, and finally provides build and deployment instructions.</p>
<h2 id="markdown-header-project-structure">Project Structure</h2>
<p>The project has been split into two maven projects with a parent owning both as shown below.</p>
<div class="codehilite"><pre>assessement-parent
|____ assessement-angular
|____ assessement-war
</pre></div>


<h2 id="markdown-header-assessement-angular">assessement-angular</h2>
<p>The assessement-angular project contains a node and bower managed javascript app that has been packaged within a maven project with the help of the <a href="https://github.com/eirslett/frontend-maven-plugin">frontend-maven-plugin</a>.  The plugin will download the specified versions of node and npm (specified as properties in the parent pom) and in the appropriate maven lifecycle phases, it will:
<em> Perform 'npm install' to install the required modules
</em> This will in turn invoke 'bower install' in order to place the required dependencies in the app folder
* Run the JavaScript tests</p>
<p>Using this plugin, we can run both Java and JavaScript tests from within Maven, while still allowing for npm and bower to be used independently during development.</p>
<p>This project has been setup to spew out a <a href="http://www.webjars.org/">web-jar</a> with the packaged JavaScript app.</p>
<h2 id="markdown-header-assessement-war">assessement-war</h2>
<p>The assessment-war project presents a RESTful HTTP API using JSON as the data bearer and implemented using Spring MVC. The API is defined in the API Description section below.  Internally, assessement-war uses Spring Data Repositories, JPA and Hibernate to save and fetch the Notes.</p>
<p>As one of its dependencies, the assessment-war has the assessement-angular project.  As described above, the assessement-angular project has been packaged as a web-jar and exposed using Spring MVC resource mapping.</p>
<p>The tests for the assessement-war project are using Spring Test utilties to set up an empty H2 database with the relevant schema, in order to test the Spring Data Repository.  </p>
<h2 id="markdown-header-api-description">API Description</h2>
<h3 id="markdown-header-note-creation">Note Creation</h3>
<h4 id="markdown-header-request">Request</h4>
<div class="codehilite"><pre><span class="n">URI</span><span class="o">:</span> <span class="n">POST</span> <span class="n">api</span><span class="o">/</span><span class="n">notes</span>
<span class="n">Body</span><span class="o">:</span> <span class="o">{</span> <span class="s2">"text"</span> <span class="o">:</span> <span class="s2">"body text here"</span><span class="o">}</span>
<span class="n">Content</span><span class="o">-</span><span class="n">Type</span><span class="o">:</span> <span class="n">application</span><span class="o">/</span><span class="n">json</span>
</pre></div>


<h4 id="markdown-header-response">Response</h4>
<div class="codehilite"><pre>HTTP Status: 201 CREATED
Content-Type: application/json
Body: { "id" : 152, "createdDate" : "10-09-2015 00:04:50", "text" : "body text here" }
</pre></div>


<h4 id="markdown-header-error-response">Error Response</h4>
<div class="codehilite"><pre>HTTP Status: 400 Bad Request
Content-Type: application/json
Body on missing text field: { "message" : "A note text is required to create a Note." }
Body on incorrect length: { "message" : "Notes text should be between 1 and 100 characters long." }
</pre></div>


<h3 id="markdown-header-note-fetching">Note Fetching</h3>
<h4 id="markdown-header-request_1">Request</h4>
<div class="codehilite"><pre><span class="n">URI</span><span class="o">:</span> <span class="n">GET</span> <span class="n">api</span><span class="o">/</span><span class="n">notes</span>
<span class="n">Params</span><span class="o">:</span> <span class="n">pageSize</span><span class="o">,</span> <span class="n">pageNumber</span><span class="o">,</span> <span class="n">ascending</span>
</pre></div>


<h4 id="markdown-header-response_1">Response</h4>
<div class="codehilite"><pre>HTTP Status: 200 OK
Content-Type: application/json
Body: {"notes" : [], "totalElements" : 0, "totalPages" : 0, "pageNumber" : 0, "elementsOnPage" : 0, "requestedPageSize" : 10, "createdDateAscendingOrder" : false }
</pre></div>


<h2 id="markdown-header-database">Database</h2>
<p>The database schema is available in the project at assessment-war/src/test/resources/db/create.sql.  The script file creates a schema called TIPCIO (if it doesn't exist, and creates a table notes within that schema.</p>
<h2 id="markdown-header-building-and-running-the-project">Building and running the project</h2>
<p>As mentioned before, the project structure allows for building, testing and packaging of the JavaScript app and the Java application through a single 'mvn verify' command issued on the assessement-parent.</p>
<p>In order to run the application, kindly follow these steps:</p>
<ol>
<li>Create the TIPICO schema and the required notes table by executing the SQL script file assessment-war/src/test/resources/db/create.sql</li>
<li>Inspect filter.properties and ensure that the JDBC connection URL, username and password are set correctly</li>
<li>Run 'mvn clean verify' on the parent project (Note: On first build, the procoess may take a long time round while node and npm are downloaded)</li>
<li>Deploy the produced war file assessment-war/target/notes.war to Tomcat</li>
<li>The app will be available at /notes/app/index.html e.g. http://localhost:8080/notes/app/index.html</li>
</ol>
<h2 id="try-id-out">Try It Out</h2>
<p>Try out the <a href="./app/index.html">Notes</> application.</p>

  </div>
</body>
</html>
